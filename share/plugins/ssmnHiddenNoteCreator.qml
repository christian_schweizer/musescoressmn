import QtQuick 2.0
import MuseScore 1.0
import QtQuick.Controls 1.0
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.1

MuseScore {
      menuPath: "Plugins.ssmnHiddenNoteCreator"
      pluginType: "dialog"
      version: "1.0"
      requiresScore: true
      id: window
      width:360
      height:50
      ExclusiveGroup { id: exclusiveGroupKey }

      property var noteValue : 16
      
      RowLayout {
            id:buttons
            x : 10
            y : 10
            Button {
                  id: btn4ths
                  width:80
                  text: "quarter"
                  onClicked: {
                        noteValue = 4
                        apply()
                        Qt.quit()
                  }
            }
            Button {
                  id: btn8ths
                  width:80
                  text: "8ths"
                  onClicked: {
                        noteValue = 8
                        apply()
                        Qt.quit()
                  }
            }
            Button {
                  id: btn16ths
                  width:80
                  text: "16ths"
                  onClicked: {
                        noteValue = 16
                        apply()
                        Qt.quit()
                  }
            }
            Button {
                  id: btn32ths
                  width:80
                  text: "32ths"
                  onClicked: {
                        noteValue = 32
                        apply()
                        Qt.quit()
                  }
            }
      }
  
      function apply() {
            var cursor = curScore.newCursor();
            cursor.rewind(1);            
            if (!cursor.segment) Qt.quit(); 

            var startTrack = cursor.track;
            var startTick = cursor.tick;
            
            // Find the end of the region we will alter
            cursor.rewind(2);
            var endTrack = cursor.track;
            var endTick = cursor.tick;
            if (endTick == 0) endTick = curScore.lastSegment.tick + 1;

            console.log(startTrack +":" + startTick + " - " + endTrack + ":" + endTick);
            var newCursor = curScore.newCursor();

            for (var track = startTrack; track <= endTrack; track += 4) {
                  console.log("\nChecking track " + track);
                  cursor.track = track;
                  newCursor.track = track;
                  cursor.rewind(1);
                  newCursor.rewind(1);
                  
                  // Process each segment
                  while (cursor.segment&& cursor.tick < endTick) {
                        if (cursor.element && cursor.element.type == Element.REST) {
                              newCursor.setDuration(1, noteValue);
                              var nbNotes = cursor.element.duration.numerator/cursor.element.duration.denominator*noteValue;
                              for(var n = 0; n < nbNotes; n++) {
                                    newCursor.addNote(42);
                              }
                              console.log(nbNotes + " notes added!");                      
                        }
                        cursor.next();
                  }      
            }
            curScore.doLayout();

            var modifyCursor = curScore.newCursor();
            //now modify the created notes
            for (var track = startTrack; track <= endTrack; track += 4) {
                  console.log("\nChecking track " + track);
                  modifyCursor.track = track;
                  modifyCursor.rewind(0);
               
                  // Process each segment
                  while (modifyCursor.segment && modifyCursor.tick < startTick) {
                        modifyCursor.next();
                  }
                  while (modifyCursor.segment && modifyCursor.tick < endTick) {
                        if (modifyCursor.element && modifyCursor.element.type == Element.CHORD) {
                              modifyCursor.element.visible = false;
                              if(modifyCursor.element.stem) {
                                    modifyCursor.element.setStemDirectionUp();
                                    modifyCursor.element.stem.visible = false;
                              }
                              if(modifyCursor.element.beam)
                                    modifyCursor.element.beam.visible = false;

                              for(var i = 0; i < modifyCursor.element.notes.length; i++) {
                                    modifyCursor.element.notes[i].visible = false;
                              }
                        }
                        modifyCursor.next();
                  }      
            }
            curScore.doLayout();

            Qt.quit()
      }

      onRun: {
            console.log("SSMN base creator")
            if (typeof curScore === 'undefined')
                  qt.Quit(); 
       }
}
